﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

using System;
using System.Threading;
using System.IO;
namespace selenium
{
    class Program
    {
        static void Main(string[] args)
        {
            System.IO.StreamReader file =
                new System.IO.StreamReader(@"C:\Users\LEHIGH_FVE.csv");  
            string line = "x";

            ChromeOptions options = null;//; = new ChromeOptions();//optional
            //options.BinaryLocation = @"C:\Program Files (x86)\BraveSoftware\Brave-Browser\Application\brave.exe";
            IWebDriver driver = null;// = new ChromeDriver("C:/Users/mikel/OneDrive/Pictures/Documents/WebDriver", options);
            //driver.Navigate().GoToUrl("https://www.pavoterservices.pa.gov/Pages/BallotTracking.aspx");
            int i = 0;
            while((line = file.ReadLine()) != null)  
            {  
                if(i == 0)
                {
                    Thread.Sleep(1000);
                    options = new ChromeOptions();//optional
                    options.BinaryLocation = @"C:\Program Files\BraveSoftware\Brave-Browser\Application\brave.exe";
                    driver = new ChromeDriver(@"C:/Users/WebDriver", options);
                    driver.Navigate().GoToUrl("https://www.pavoterservices.pa.gov/Pages/BallotTracking.aspx");
                }
                string[] fields = line.Split(',');
                var results = "";
                try
                {
                    WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
                    IWebElement FSElement = wait.Until(driver => driver.FindElement(By.Name("ctl00$ContentPlaceHolder1$FirstNameText")));
                    FSElement.SendKeys(Keys.Control + "A" + Keys.Backspace);
                    FSElement.SendKeys(fields[0]+ Keys.Tab);
                    Console.Write("First Name Done");

                    IWebElement LSElement = wait.Until(driver => driver.FindElement(By.Name("ctl00$ContentPlaceHolder1$LastNameText")));
                    LSElement.SendKeys(Keys.Control + "A" + Keys.Backspace);
                    LSElement.SendKeys(fields[1]+ Keys.Tab);
                    Console.Write("Last Name Done");

                    IWebElement DOBElement = wait.Until(driver => driver.FindElement(By.Name("ctl00$ContentPlaceHolder1$DateOfBirthText")));
                    DOBElement.SendKeys(Keys.Control + "A" + Keys.Backspace);
                    DOBElement.SendKeys(fields[2]+ Keys.Tab + "Lehigh");
                    Console.Write("DOB Done");

                    IWebElement RetElement = wait.Until(driver => driver.FindElement(By.Name("ctl00$ContentPlaceHolder1$RetrieveButton")));
                    RetElement.SendKeys(Keys.Return);
                    Console.Write("Button Click Done");     
                
                }
                catch(Exception e)
                {
                    results = "0,0,0,0,0,0,Not registered";
                }

                try
                {
                    results = driver.FindElement(By.XPath("//*[@id=\"ctl00_ContentPlaceHolder1_ResultPanel\"]/div/div/div/table/tbody/tr[3]")).Text;
                    results = results.Replace("\r", ",");
                    results = results.Replace("\n", "\b");
                    results = results.Replace("\b", "");
                }
                catch (Exception ex)
                {
                    results = "0,0,0,0,0,0,Not registered";
                }                

                WriteToFile(results, "RESULTS", "csv") ;
                i++;
                if(i > 20)
                {
                    i = 0;
                    driver.Quit();
                }
            }
        }

        static void WriteToFile(String value, String filename,String fileExtention)
        {
            //opening txt file
            var file = new FileStream(filename+"."+fileExtention, FileMode.Append);
            var sw = new StreamWriter(file);

            //writing to the file
            sw.WriteLine(value);
            //closing file
            sw.Close();
        }
    }
}